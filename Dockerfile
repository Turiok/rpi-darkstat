# Builds a docker image for a darkstat
FROM debian:stable-slim as builder

ARG DARKSTAT_VERSION

###############################################
##           ENVIRONMENTAL CONFIG            ##
###############################################
# Set correct environment variables
ENV DEBIAN_FRONTEND noninteractive
ENV HOME="/root" LC_ALL="C.UTF-8" LANG="en_US.UTF-8" LANGUAGE="en_US.UTF-8"

WORKDIR /
###############################################
##   INSTALL ENVIRONMENT                     ##
###############################################

RUN apt-get update && apt-get install -y zlib1g-dev \
        libpcap-dev git autoconf \
        build-essential vim-common

#########################################
##             COMPILATION             ##
#########################################
COPY darkstat /darkstat

RUN cd /darkstat \
    && mkdir build \
    && autoconf \
    && autoheader \
    && ./configure --prefix=$PWD/build/ \
    && make \
    && make install

###############################################
###############################################
##   PRODUCTION                              ##
###############################################

FROM ${ARCH}debian:stable-slim as production
MAINTAINER Didier C <crd.compte@gmail.com>

###############################################
##           ENVIRONMENTAL CONFIG            ##
###############################################
# Set correct environment variables
ENV DEBIAN_FRONTEND noninteractive
ENV HOME="/root" LC_ALL="C.UTF-8" LANG="en_US.UTF-8" LANGUAGE="en_US.UTF-8"

WORKDIR /

###############################################
##   INSTALL ENVIRONMENT                     ##
###############################################
# Add these repositories for following deb installation.
RUN apt-get update && apt-get install -y libpcap0.8 \
    && rm -rf /var/lib/apt/lists/* /var/cache/* /var/tmp/*

COPY --from=0 /darkstat/build darkstat
COPY run.sh /run.sh
RUN chmod +x /run.sh


VOLUME ["/darkstat/config"]
EXPOSE 667
CMD ["/run.sh"]



